<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * @todo поискать использование
 */
$app->get('/check_user_role', function (Request $request) use ($app) {
    return $app->json($app['security']->getToken()->getUser());
});

/**
 * @todo поискать использование
 */
$app->get('/user', function (Request $request) use ($app) {
    $token = $app['security']->getToken();
    if (null !== $token) {
        $user = $token->getUser();
        $response = [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
        ];
    } else {
        $response = [];
    }

    return $app->json($response);
});
