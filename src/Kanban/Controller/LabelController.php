<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LabelController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $boardId  = $request->query->get('board_id');
        $response = $this->app['gitlab_api']->executeCommand('GetLabels', ['project_id'=>$boardId]);
        return $response;
    }
}