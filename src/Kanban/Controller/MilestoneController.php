<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MilestoneController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $projectId = $request->query->get('project_id', 0);
        $state = $request->query->get('state', 'active');

        $response = $this->app['gitlab_api']->executeCommand('GetMilestones', ['project_id' => $projectId, 'per_page' => 1000]);

        return array_filter(iterator_to_array($response), function($milestone){
            return $milestone->getState() != 'closed';
        });
    }
}
