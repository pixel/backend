<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $project_id = $request->query->get('project_id');
        $issue_id = $request->query->get('issue_id');

        $response = $this->app['gitlab_api']->executeCommand('GetComments', [
            'project_id' => $project_id,
            'issue_id' => $issue_id,
        ]);

        $response = iterator_to_array($response);

        usort($response, function($a, $b){
            return $a->getCreatedAt()->getTimestamp() > $b->getCreatedAt()->getTimestamp();
        });

        return $response;
    }


    public function createAction(Request $request)
    {
        $response = $this->app['gitlab_api']->executeCommand('CreateComment',
            $request->request->all()
        );

        return $response;
    }

    public function updateAction(Request $request)
    {
        $response = $this->app['gitlab_api']->executeCommand('EditComment',
            $request->request->all()
        );

        return $response;
    }
}