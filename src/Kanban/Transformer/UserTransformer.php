<?php

namespace Kanban\Transformer;

use Gitlab\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $result = [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'name'  => $user->getName(),
            'avatar_url' => $user->getAvatarUrl(),
        ];

        return $result;
    }
}
