<?php

namespace Kanban\Transformer;

use League\Fractal\TransformerAbstract;
use Gitlab\Models\Milestone;

class MilestoneTransformer extends TransformerAbstract 
{
    public function transform(Milestone $milestone)
    {
        $result = [
            'id' => $milestone->getId(),
            'title' => $milestone->getTitle(),
            'state' => $milestone->getState(),
        ];

        return $result;
    }
}

