<?php

namespace Kanban\Transformer;

use Gitlab\Models\Group;
use League\Fractal\TransformerAbstract;

class GroupTransformer extends TransformerAbstract
{
    public function transform(Group $group)
    {
        $result = [
            'id'       => $group->getId(),
            'name'     => $group->getName(),
            'path'     => $group->getPath(),
            'owner_id' => $group->getOwnerId(),
        ];

        return $result;
    }
}

