<?php

namespace Kanban\Component\Provider;

use Kanban\Component\Transport\Amqp;
use Pimple\ServiceProviderInterface;

/**
 * Created by PhpStorm.
 * User: mac
 * Date: 12/01/15
 * Time: 09:42
 */

class AmqpServiceProvider implements ServiceProviderInterface
{

    public function register(\Pimple\Container $pimple)
    {
        $pimple['core.mq'] = function () use ($pimple) {
            return new Amqp($pimple['amqp.config']);
        };
    }
}