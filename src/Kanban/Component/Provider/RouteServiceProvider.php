<?php

namespace Kanban\Component\Provider;

use Kanban\Component\Transport\Amqp;
use Pimple\ServiceProviderInterface;
use Symfony\Component\Routing\Route;

/**
 * Created by PhpStorm.
 * User: mac
 * Date: 12/01/15
 * Time: 09:42
 */

class RouteServiceProvider implements ServiceProviderInterface
{

    public function register(\Pimple\Container $pimple)
    {
        $options = $pimple['router.options'];

        foreach ($options['routes'] as $name => $route) {
            $pimple['routes']->add(
                $name,
                new Route(
                    $options['prefix'].$route['path'],
                    $route['defaults'],
                    [],
                    isset($route['options']) ? $route['options'] : [],
                    '',
                    [],
                    $route['methods']
                )
            );
        }
    }
}