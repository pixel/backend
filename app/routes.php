<?php

$baseRoute = array(
    'milestones'     => [
        'path'     => '/milestones',
        'defaults' => [
            '_controller' => 'milestone.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Collection',
                'class' => 'MilestoneTransformer'
            ],
        ]
    ],

    'groups'     => [
        'path'     => '/groups',
        'defaults' => [
            '_controller' => 'group.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Collection',
                'class' => 'GroupTransformer'
            ],
        ]
    ],

    'labels'     => [
        'path'     => '/labels',
        'defaults' => [
            '_controller' => 'label.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Collection',
                'class' => 'LabelTransformer'
            ],
        ]
    ],

    'users'     => [
        'path'     => '/users',
        'defaults' => [
            '_controller' => 'user.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ]
    ],
);

return array_merge(
    require_once 'routes/board.php',
    require_once 'routes/card.php',
    require_once 'routes/comment.php',
    require_once 'routes/security.php',
    $baseRoute
);