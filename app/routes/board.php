<?php

return array(
    'boards'          => [
        'path'     => '/boards',
        'defaults' => [
            '_controller' => 'board.controller:getCollectionAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'collection',
                'class' => 'BoardTransformer'
            ],
        ]
    ],

    'board'          => [
        'path'     => '/board',
        'defaults' => [
            '_controller' => 'board.controller:getAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Item',
                'class' => 'BoardTransformer'
            ],
        ]
    ],

    'board.events'          => [
        'path'     => '/board/events',
        'defaults' => [
            '_controller' => 'board.controller:getEventsAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'Item',
                'class' => 'BoardEventsTransformer'
            ],
        ]
    ],

    'board.configure' => [
        'path'     => '/boards/configure',
        'defaults' => [
            '_controller' => 'board.controller:postConfigureAction'
        ],
        'methods'  => [
            'POST'
        ],
    ],
);