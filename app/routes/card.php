<?php

return array(
    'cards'           => [
        'path'     => '/cards',
        'defaults' => [
            '_controller' => 'card.controller:indexAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'collection',
                'class' => 'CardTransformer'
            ],
        ]
    ],
    'card'            => [
        'path'     => '/card',
        'defaults' => [
            '_controller' => 'card.controller:cardAction'
        ],
        'methods'  => [
            'GET'
        ],
        'options' => [
            'response.type' => 'json',
            'transformer' => [
                'type' => 'item',
                'class' => 'CardTransformer'
            ],
        ]
    ],

    'card.create'     => [
        'path'     => '/card',
        'defaults' => [
            '_controller' => 'card.controller:createAction'
        ],
        'methods'  => [
            'POST'
        ],
        'options' => [
            'response.type' => 'ws',
            //'response.type' => 'json',
            'transformer' => [
                'type' => 'item',
                'class' => 'CardTransformer'
            ],
        ]
    ],

    'card.update'     => [
        'path'     => '/card',
        'defaults' => [
            '_controller' => 'card.controller:updateAction'
        ],
        'methods'  => [
            'PUT'
        ],
        'options' => [
            'response.type' => 'ws',
            'transformer' => [
                'type' => 'item',
                'class' => 'CardTransformer'
            ],
        ]
    ],

    'card.move'     => [
        'path'     => '/card/move',
        'defaults' => [
            '_controller' => 'card.controller:moveToAction'
        ],
        'methods'  => [
            'PUT'
        ],
        'options' => [
            'response.type' => 'ws',
            'transformer' => [
                'type' => 'item',
                'class' => 'CardTransformer'
            ],
            'includes' => [
                'project'
            ]
        ]
    ],

    'card.delete'     => [
        'path'     => '/card',
        'defaults' => [
            '_controller' => 'card.controller:deleteAction'
        ],
        'methods'  => [
            'DELETE'
        ],
        'options' => [
            'response.type' => 'ws',
            'transformer' => [
                'type' => 'item',
                'class' => 'CardTransformer'
            ],
        ]
    ],
);