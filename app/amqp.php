<?php

return [
    "connection"     => array(
        "host"     => "rabbitmq",
        "port"     => 5672,
        "login"    => "guest",
        "password" => "guest",
    ),
    "publishersArguments" => array(
        "default" => array(
            "delivery_mode" => 2,
            "content-type" => "text/plain",
            "content-encoding" => "base64",
        ),
    ),
    "defaultPublisherArguments" => array(
        "delivery_mode"    => 1,
        "content_type"     => "text/plain",
        "content_encoding" => "base64",
    ),
    "exchanges"      => array(
        "default" => array(
            "name"      => "amq.topic",
            "type"      => AMQP_EX_TYPE_TOPIC,
            "flags"     => AMQP_DURABLE,
            "arguments" => array(),
        )
    ),
    "routingKeys" => array(
        "default" => "requests"
    ),
];